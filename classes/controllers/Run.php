<?php

class Run_Controllers{
	protected $_db;
	protected $_template;
	protected $_customer;
	protected $_events;

	public function init(){
		$this->_db = new DB_Models();
		$this->_template = new Templates_Controllers();
		$this->_customer = new Login_Controllers($this->_db);
		$this->_events =  new Events_Controllers($this->_db);

	}

	public function view(){
		$page = 'index';//isset($_SESSION['customer'])?'calendar':'index';
		return $this->_template->pageView($page);
	}

	public function getContent($fileName){
		return $this->_template->loadContent($fileName);
	}

	public function getAuth($data){
		$data['pass'] = md5($data['pass']);
		$result = $this->_customer->checkUser($data);
		if($result){
			$_SESSION['customer'] = array_pop($result);
			return $result;
		}
		return array('error'=>'User not found.');
	}

	public function getEvents(){
		return $this->_events->getAllEvents();
	}

	public function sendInviteEmail($params){
		var_dump($params);
		$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
		$invite_code = md5($params['invite']);
		$this->_customer->insertInvite(array('customer_email'=>$params['invite'],'invite_code'=>$invite_code));
		mail($params['invite'], "Invite to calendar", 'Your invite link is: '.$actual_link.'/invite_link='.$invite_code);
		
	}

	public function deleteEvent($params){
		$this->_events->deleteEvent($params);
	}

	public function addCustomer($link){
		$this->_customer->addCustomer($link);
	}

	public function addEvent($params){
		$this->_events->insertEvent($params);
	}
}