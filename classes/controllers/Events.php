<?php
class Events_Controllers{
		protected $_db;

		public function __construct($db){
			$this->_db = $db;
		}

		public function getAllEvents(){
			return $this->_db->querySelect('events');
		}


		public function insertEvent($data){
			$data['id'] = null;
			$data['customer_id'] = $_SESSION['customer']['id'];
			$this->_db->queryInsert('events', $data);
		}

		public function deleteEvent($data){
			$this->_db->queryDelete('events', array('id'=>$data));
		}
}
?>