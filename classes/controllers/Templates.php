<?php

	class Templates_Controllers{

		protected $_template = 'views/template.phtml';

		protected function loadFile($fileName){
			ob_start();
		    include($fileName);
		    $pageContent = ob_get_contents();
		    ob_end_clean();

		    return $pageContent;
		}

		protected function loadTemplate(){
	        return $this->loadFile($this->_template);
		}

		public function loadContent($fileName){
			$path = 'views/'.$fileName.'.phtml';
			if(!file_exists($path)){
				return '';
			}

			return $this->loadFile($path);
		}

		protected function getPageCode($fileName){
			$template = $this->loadTemplate();
			$content = $this->loadContent($fileName);
			return str_replace('{{content}}', $content, $template);
		}

		public function pageView($fileName){
			return $this->getPageCode($fileName);
		}
	}
?>