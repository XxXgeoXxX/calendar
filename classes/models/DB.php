<?php

	class DB_Models{

		protected $_host = 'localhost';
		protected $_db = 'tz_eng';
		protected $_login = 'root';
		protected $_pass = '';

		protected $_pdo;

		public function __construct(){
			$this->_pdo = new PDO(
				'mysql:dbname='.$this->_db.';host='.$this->_host,
				$this->_login,
				$this->_pass
			);
		}

		public function querySelect($table, $fields = array(), $params = array()){
			$columns =  count($fields)? explode(',', $fields): '*';
			$query = 'select '.$columns.' from '.$table;
			$query.= count($params)? ' where '.$this->prepareConditions($params): '';
			$result = $this->_pdo->prepare($query);
			if(count($params)){
				$result->execute($this->prepareParams($params));
			}
			else{
				$result->execute();
			}
			
			$data = $result->fetchAll(PDO::FETCH_ASSOC);
			if(!$data){
				return false;
			}
			return $data; 
		}

		protected function prepareParams($params){
			if(!count($params)){
				return '';
			}
			$result = array();
			foreach($params as $key => $value){
				$result[':'.$key] = $value;
			}
			return $result;
		}

		protected function prepareConditions($params){

			$conditions = '';
			foreach($params as $key => $value){
				$conditions.= $key.'=:'.$key.' and ';
			}
			return substr($conditions, 0, -4);
		}
		protected function prepareValue($params){
			$values = '';
			foreach($params as $key => $value){
				$conditions.= ':'.$key.', ';
				
			}
			return substr($conditions, 0, -2);
		}

		public function queryInsert($table, $params){ 
			$sql = 'insert into '.$table.'('.implode(',', array_keys($params)).') values('.$this->prepareValue($params).')';
			$stmt = $this->_pdo->prepare($sql);
                                                                                      
			$stmt->execute($this->prepareParams($params)); 
		}

		public function queryDelete($table, $params = array()){
			echo $table;
			$sql =	'delete from '.$table;
			$sql.= count($params)? ' where '.$this->prepareConditions($params): '';
			$stmt = $this->_pdo->prepare($sql);
	  		
			$stmt->execute($this->prepareParams($params));
		}

		public function queryUpdate($table, $params = array()){
			
		}
	}