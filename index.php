<?php
	session_start();
	spl_autoload_register(function($className){
		$pathArray = explode('_', $className);
		include_once('classes/'.strtolower(array_pop($pathArray)).'/'.implode('/', $pathArray).'.php');
	});
	
	$run = new Run_Controllers();
	$run->init();
	if(isset($_GET['invite_link'])){
		$run->addCustomer($_GET['invite_link']);

	}

	if(isset($_POST['tz_eng'])){
		$params = json_decode($_POST['tz_eng'], true);
		reset($params);
		$key = key($params);
		switch($key){
			case 'login': 
				$result = $run->getAuth($params);
				$template =  $run->getContent('calendar');
				$events =  $run->getEvents();
				echo json_encode(array('result'=>$result, 'template'=>$template, 'events'=>$events, 'customer'=>$_SESSION['customer']));
			break;

			case 'invite': 
				$result = $run->sendInviteEmail($params);
			break;

			case 'deleteEvent': 
				$run->deleteEvent($params['deleteEvent']);
			break;

			case 'addEvent': 
				$run->addEvent($params['addEvent']); 
			break;

		}
		
	}
	else{
		echo $run->view();
	}
?>