
class Send{

	checkUser(form){
		
		document.body.querySelector('.error').innerHTML = '';

		if(form.login.checkValidity() && form.pass.checkValidity()){
			return true;
		}
			form.login.reportValidity();
			form.pass.reportValidity();
		
	}

	sendData(data){
		this.xhr = new XMLHttpRequest();
		this.xhr.open('post', 'index.php', true);
		this.xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		this.xhr.send('tz_eng=' + JSON.stringify(data));
		this.xhr.onload = function(){
			var data = JSON.parse(this.response);
			if(data.result.error){
				document.body.querySelector('.error').innerHTML = data.result.error;
			}
			else{

				document.body.querySelector('.content').innerHTML = data.template;
				var scripts = document.body.querySelector('.content').getElementsByTagName('script');
				var scriptsCount = scripts.length;
				if(scriptsCount){
					for(var i=0; i<scriptsCount; i++){
						var xhrScript = new XMLHttpRequest();
						xhrScript.open('post', scripts[i].src, true);
						xhrScript.send();
						xhrScript.onload = function(){ 
							for(var key in data.events){
								var time = data.events[key].time.split('-');
								data.events[key].title = data.events[key].name;
								data.events[key].start = data.events[key].date+' '+time[0];
								data.events[key].start = data.events[key].date+' '+time[1];
							}
							var code = this.responseText;
							var replaced = {'{{event}}':  JSON.stringify(data.events),
											'{{customer_id}}': data.customer.id,
											'{{is_admin}}': parseInt(data.customer.admin_key)
											};
							for(var key in replaced){
								code = code.replace(key, replaced[key]);
							}
							eval(code);
						};
					}
				}
				
			}
			
		};
	}

}