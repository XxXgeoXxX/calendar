
var events = {{event}};


$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay,listMonth'
			},

			// customize the button names,
			// otherwise they'd all just say "list"
			views: {
				listDay: { buttonText: 'day' },
				listWeek: { buttonText: 'week' }
			},
	theme: true,
	defaultDate: new Date(),
	navLinks: true,
	editable: true,
	eventLimit: true, // allow "more" link when too many events
	events: events, 
	eventClick: function(calEvent, jsEvent, view) {
		alert("Title:"+calEvent.title+'\nDescription: '+calEvent.description);      
    },
    eventRender: function(event, element) {
     	element.find(".fc-bg").remove();
     	 	element.find(".fc-time").prepend(event.status+' ');
     	 	console.log(event)
     	 	if(event.customer_id == {{customer_id}} || {{is_admin}} ){
     	 		element.append( "<span class='closeon' data-event-id='"+event._id+"'>X</span>" );
            	element.find(".closeon").click(function() {
               		$('#calendar').fullCalendar('removeEvents',event._id);
        		});
     	 	}
            
    },
    eventDragStop: function( event, jsEvent, ui, view) { 
    	console.log(jsEvent);
    },
    eventResizeStop: function( event, jsEvent, ui, view ) {
		console.log(arguments);
    }
});
$('#calendar').fullCalendar( 'clientEvents', function(){console.log(this)} );
