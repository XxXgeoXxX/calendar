"use strict"

window.onload = function(){
	document.body.addEventListener('click', function(event) {
		var send = new Send();
		switch(event.target.className){
			case 'checkUser': checkUser(send); break;
			case 'inviteEmail': sendEmail(send); break;
			case 'closeon': deleteEvent(send, event.target); break;
			case 'sendEvent': addEvent(send);

		}

	});
	
	function checkUser(send){
		var form = document.forms['auth'].elements;
 		if(send.checkUser(form)){
 				send.sendData({'login': form.login.value, 'pass': form.pass.value});
 		}
	}

	function sendEmail(send){
		send.sendData({'invite': document.forms['invite'].elements.invite.value});
		document.forms['invite'].elements.invite.value = '';
	}

	function deleteEvent(send, e){
		send.sendData({'deleteEvent': e.getAttribute('data-event-id')});
	}

	function addEvent(send){
		var data = {};
		var form = document.forms['calendar'].elements;
		for(var key in form){
			if(form[key].value == 'Send'){
				break;
			}
			data[form[key].name] = form[key].value;
			form[key].value = '';
		}
		console.log(send);
		send.sendData({'addEvent': data});
	}
}